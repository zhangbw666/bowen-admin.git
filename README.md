**项目说明** 

## bowen-admin
- bowen-admin基于vue、element-ui构建开发，实现bowen-ware后台管理前端功能，提供一套更优的前端解决方案。

<br> 

**项目特点**
- 前后端分离，通过token进行数据交互，可独立部署。
- 主题定制，通过scss变量统一一站式定制。
- 动态菜单，通过菜单管理统一管理访问路由。
- 数据切换，通过mock配置对接口数据／mock模拟数据进行切换。
- 发布时，可动态配置CDN静态资源／切换新旧版本。

<br> 

 **项目部署**
 - 本项目是前后端分离，先部署后端项目（见bowen-ware项目），再部署前端才能运行起来。
 - 通过git下载源码（git clone https://gitee.com/zhangbw666/bowen-admin.git）
 - 开发环境
  ```
 - 进入Idea Terminal终端：分别执行 npm install、npm run dev即可启动。
 - 前端部署完毕，就可以访问项目了。URL：http://localhost:8008/#/login 账号：admin 密码：123456
  ```
 - 生产环境
 ```
 # 构建生产环境(默认)
   npm run build
   
 # 构建测试环境
   npm run build --qa
   
 # 构建验收环境
   npm run build --uat
   
 # 构建生产环境
   npm run build --prod
   
 # 安装Nginx,并配置Nginx
   server {
           listen       81;
           server_name  localhost;
   
           location / {
               root   dist;
               index  index.html index.htm;
           }
   }
 # 启动Nginx后，访问如下路径即可 http://localhost:81 账号：admin 密码：123456
 # 说明：1、将前端打包生成的静态文件dist放到nginx里并和conf目录同级  
         2、将dist文件里的static目录移到与conf同级位置，并删除有版本号的文件夹、静态文件只保留static、config、index.html  
         3、修改config文件夹下的index.js文件，修改后的文件如下：  
         // cdn地址 = 域名 + 版本号
         window.SITE_CONFIG['domain']  = './'; // 域名
         //window.SITE_CONFIG['version'] = '2007061021';   // 版本号(年月日时分)
         //window.SITE_CONFIG['cdnUrl']  = window.SITE_CONFIG.domain + window.SITE_CONFIG.version;
         //cdn地址去掉版本号
         window.SITE_CONFIG['cdnUrl']  = window.SITE_CONFIG.domain;
```
 - 问题解决
  ```
 - 打包报错：Module build failed: Error: ENOENT: no such file or directory, scandir 'D:\code\bowen-admin\node_mod
 - 解决方式
 - npm rebuild node-sass
 - npm install chromedriver --chromedriver_cdnurl=http://cdn.npm.taobao.org/dist/chromedriver
 
 - gulp版本语法冲突
 - 解决方式
 - npm install --save-dev gulp@3.9.1
 - https://www.liquidlight.co.uk/blog/how-do-i-update-to-gulp-4/
 - npm i optimize-css-assets-webpack-plugin@3.2.0
 
 - node-sass安装报错node-sass@4.12.0 postinstall: `node scripts/build.js`
 - 解决方式
 - cnpm install node-sass --save
  ```

<br>

 **登录页面**  
 ![登录页面1](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-ware/login.jpg?raw=true)  
